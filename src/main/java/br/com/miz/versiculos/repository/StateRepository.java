package br.com.miz.versiculos.repository;

import br.com.miz.versiculos.domain.StateId;
import br.com.miz.versiculos.domain.telegram.State;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface StateRepository extends CrudRepository<State, Long> {
    Optional<State> findStateByStateId(StateId stateId);
}
