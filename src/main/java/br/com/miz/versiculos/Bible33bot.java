package br.com.miz.versiculos;

import br.com.miz.versiculos.domain.*;
import br.com.miz.versiculos.domain.telegram.Commands;
import br.com.miz.versiculos.domain.telegram.Emoji;
import br.com.miz.versiculos.domain.telegram.State;
import br.com.miz.versiculos.repository.StateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendAnimation;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardRemove;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
public class Bible33bot extends TelegramLongPollingBot {

    private static final int STARTSTATE = 0;
    private static final int MAINMENU = 1;
    private static final int VERSE = 2;
    private static final int VERSION = 3;
    private static final int CHAPTER = 4;
    private static final int VERSE_NUMBER = 5;

    @Value("${botToken}")
    private String botToken;
    @Value("${botUsername}")
    private String botUsername;

    private StateRepository stateRepository;
    private BibleService bibleService;

    @Autowired
    public Bible33bot(StateRepository stateRepository, BibleService bibleService) {
        this.stateRepository = stateRepository;
        this.bibleService = bibleService;
    }

    @Override
    public void onUpdateReceived(Update update) {
        try {
            if (update.hasMessage()) {
                Message message = update.getMessage();
                if (message.hasText() || message.hasLocation()) {
                    handleIncomingMessage(message);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Check if there is a Callback
        if (update.hasCallbackQuery()) {
            try {
                System.out.println(update.getCallbackQuery());
                handleIncomingCallback(update.getCallbackQuery());
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }

        }


        System.out.println(update);
    }

    private void handleIncomingCallback(CallbackQuery callbackQuery) throws TelegramApiException {
        // Pega o estado atual do chat
        final int state = getState(callbackQuery.getFrom().getId(), callbackQuery.getMessage().getChatId());
        switch(state) {
            case MAINMENU:
                execute(onVerseChoosen(callbackQuery, state));
                break;
            case VERSE:
                execute(onVersionChoosen(callbackQuery, state));
                break;
            case VERSION:
                execute(onBookChoosen(callbackQuery, state));
                break;
            case CHAPTER:
                execute(onChapterChoosen(callbackQuery, state));
                break;
            default:
                System.out.println("Callback sem previsão");
                break;
        }
    }

    private void handleIncomingMessage(Message message) throws TelegramApiException {
        // Pega o estado atual do chat
        final int state = getState(message);
        if (!message.isUserMessage() && message.hasText()) {
            if (message.getText().startsWith(Commands.STOPCOMMAND))
                sendHideKeyboard(message.getFrom().getId(), message.getChatId(), message.getMessageId());
        }
        BotApiMethod<Message> response = null;
        switch(state) {
            default:
                response = sendMessageDefault(message);
                break;
        }
        Message sentMessage = null;
        if (response != null)
             sentMessage = execute(response);
        updateLastBotMessage(message, sentMessage.getMessageId());
    }

    private EditMessageText onVerseChoosen(CallbackQuery callbackQuery, int state) {
        EditMessageText editMessageText = new EditMessageText();
        editMessageText.enableMarkdown(true);

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();

        List<InlineKeyboardButton> firstInlineKeyboardButtons = new ArrayList<>();
        InlineKeyboardButton versionButton = new InlineKeyboardButton(getVersionCommand());
        versionButton.setCallbackData("nvi");
        firstInlineKeyboardButtons.add(versionButton);
        inlineKeyboardMarkup.setKeyboard(Collections.singletonList(firstInlineKeyboardButtons));

        saveState(callbackQuery.getFrom().getId(), callbackQuery.getMessage().getChatId(), VERSE);

        editMessageText.setReplyMarkup(inlineKeyboardMarkup);
        editMessageText.setChatId(callbackQuery.getMessage().getChatId());
        editMessageText.setMessageId(callbackQuery.getMessage().getMessageId());
        editMessageText.setText("Escolha uma versão:");

        return editMessageText;
    }

    private EditMessageText onVersionChoosen(CallbackQuery callbackQuery, int state) {
        String version = callbackQuery.getData();
        EditMessageText editMessageText = new EditMessageText();
        editMessageText.enableMarkdown(true);
        Map<String, String> books = bibleService.getBible(version)
                .map(b -> bibleService.getBooksAbbrev(b, true)).orElse(new HashMap<>());

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> line = new ArrayList<>();
        List<String> abrevs = new ArrayList<>(books.keySet());
        for (int i = 0; i < books.size(); i++) {
            String abrev = abrevs.get(i);
            InlineKeyboardButton bookButton = new InlineKeyboardButton(books.get(abrev));
            bookButton.setCallbackData(String.format("%s-%s", version, abrev));
            line.add(bookButton);
            if (i != 0 && i % 3 == 0) {
                rows.add(line);
                line = new ArrayList<>();
            } else if (i == books.size() -1) {
                rows.add(line);
            }
        }
        inlineKeyboardMarkup.setKeyboard(rows);
        saveState(callbackQuery.getFrom().getId(), callbackQuery.getMessage().getChatId(), VERSION);

        editMessageText.setReplyMarkup(inlineKeyboardMarkup);
        editMessageText.setChatId(callbackQuery.getMessage().getChatId());
        editMessageText.setMessageId(callbackQuery.getMessage().getMessageId());
        editMessageText.setText("Escolha um Livro:");

        return editMessageText;
    }

    private EditMessageText onBookChoosen(CallbackQuery callbackQuery, int state) {
        EditMessageText editMessageText = new EditMessageText();
        editMessageText.enableMarkdown(true);
        String[] callbackdata = callbackQuery.getData().split("-");
        String version = callbackdata[0];
        String bookAbr = callbackdata[1];

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();

        Book book = bibleService.getBible(version)
                .map(Bible::getBooks).orElse(new ArrayList<>()).stream()
                .filter(b -> b.getAbbrev().equalsIgnoreCase(bookAbr))
                .findFirst().orElse(new Book());

        List<InlineKeyboardButton> line = new ArrayList<>();
        for (int i = 1; i <= book.getChapters().size(); i++) {
            InlineKeyboardButton chapterButton = new InlineKeyboardButton(String.valueOf(i));
            chapterButton.setCallbackData(String.format("%s-%s-%s", version, book.getAbbrev(), i));
            line.add(chapterButton);
            if (i % 4 == 0) {
                rows.add(line);
                line = new ArrayList<>();
            } else if (i == book.getChapters().size()) {
                rows.add(line);
            }
        }
        inlineKeyboardMarkup.setKeyboard(rows);
        saveState(callbackQuery.getFrom().getId(), callbackQuery.getMessage().getChatId(), CHAPTER);

        editMessageText.setReplyMarkup(inlineKeyboardMarkup);
        editMessageText.setChatId(callbackQuery.getMessage().getChatId());
        editMessageText.setMessageId(callbackQuery.getMessage().getMessageId());
        editMessageText.setText("Escolha um Capítulo:");

        return editMessageText;
    }

    private SendMessage onChapterChoosen(CallbackQuery callbackQuery, int state) {
        SendMessage sendMessageText = new SendMessage();
        String[] callbackdata = callbackQuery.getData().split("-");
        String version = callbackdata[0];
        String bookAbr = callbackdata[1];
        String chapter = callbackdata[2];

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();

        Book book = bibleService.getBible(version)
                .map(Bible::getBooks).orElse(new ArrayList<>()).stream()
                .filter(b -> b.getAbbrev().equalsIgnoreCase(bookAbr))
                .findFirst().orElse(new Book());

        List<Verse> verses = new ArrayList<>();
        List<MapperVerse> mapperVerses = book.getChapter(Integer.parseInt(chapter));
        for (int i = 0; i < mapperVerses.size(); i++) {
            verses.add(new Verse(book, Integer.parseInt(chapter), i+1, mapperVerses.get(i).getText()));
        }
        String chapterText =  verses.stream()
                .map(v -> String.format("%s- %s", v.getNumber(), v.getText()))
                .collect(Collectors.joining(" ", "", String.format(" (%s %s-%s)", book.getName(), 1, verses.size())));

        saveState(callbackQuery.getFrom().getId(), callbackQuery.getMessage().getChatId(), MAINMENU);
        sendMessageText.setChatId(callbackQuery.getMessage().getChatId());
        sendMessageText.setText(chapterText);

        return sendMessageText;
    }

    private String getVersionCommand() {
        return "NVI";
    }

    private SendMessage sendMessageDefault(Message message) {
        InlineKeyboardMarkup inlineKeyboardMarkup = getMainMenuKeyboard();
        saveState(message, MAINMENU);
        return sendHelpMessage(message.getChatId(), message.getMessageId(), inlineKeyboardMarkup);
    }

    private SendMessage sendHelpMessage(long chatId, int messageId, InlineKeyboardMarkup inlineKeyboardMarkup) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(chatId);
        //sendMessage.setReplyToMessageId(messageId);
        if (inlineKeyboardMarkup != null) {
            sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        }
        sendMessage.setText(getHelpMessage());
        return sendMessage;
    }

    private String getHelpMessage() {
        return "Escolha uma das opções abaixo";
    }

    private InlineKeyboardMarkup getMainMenuKeyboard() {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<InlineKeyboardButton> firstInlineKeyboardButtons = new ArrayList<>();
        InlineKeyboardButton versionButton = new InlineKeyboardButton(getVerseCommand());
        versionButton.setCallbackData("verse");
        firstInlineKeyboardButtons.add(versionButton);
        inlineKeyboardMarkup.setKeyboard(Collections.singletonList(firstInlineKeyboardButtons));
        return inlineKeyboardMarkup;
    }

    private String getCurrentCommand() {
        return Emoji.BLACK_RIGHT_POINTING_TRIANGLE.toString();
    }
    private String getVerseCommand() {
        return String.format("Ler capítulo %s", Emoji.BLACK_RIGHT_POINTING_DOUBLE_TRIANGLE.toString());
    }

    private SendMessage sendHideKeyboard(Integer userId, Long chatId, Integer messageId) throws TelegramApiException {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.enableMarkdown(true);
        sendMessage.setReplyToMessageId(messageId);
        sendMessage.setText(Emoji.WAVING_HAND_SIGN.toString());

        ReplyKeyboardRemove replyKeyboardRemove = new ReplyKeyboardRemove();
        replyKeyboardRemove.setSelective(true);
        sendMessage.setReplyMarkup(replyKeyboardRemove);

        saveState(userId, chatId, STARTSTATE);

        return sendMessage;
    }

    @Override
    public String getBotUsername() {
        return botUsername;
    }

    @Override
    public String getBotToken() {
        return botToken;
    }

    private void saveState(Message message, int state) {
        saveState(message.getFrom().getId(), message.getChatId(), state);
    }

    private void saveState(int userId, long chatId, int state) {
        saveState(userId, chatId, state, getLastBotMessage(userId, chatId));
    }

    private void updateLastBotMessage(Message message, int lastBotMessage) {
        stateRepository.findStateByStateId(new StateId(message.getFrom().getId(), message.getChatId()))
        .ifPresent(state -> {
            state.setLastBotMessage(lastBotMessage);
            stateRepository.save(state);
        });
    }

    private void saveState(int userId, long chatId, int state, int lastBotMessage) {
        stateRepository.save(new State(new StateId(userId, chatId), state, lastBotMessage));
    }

    private int getState(Message message) {
        return stateRepository.findStateByStateId(new StateId(message.getFrom().getId(), message.getChatId()))
                .map(State::getState).orElse(0);
    }

    private int getState(int userId, long chatId) {
        return stateRepository.findStateByStateId(new StateId(userId, chatId))
                .map(State::getState).orElse(0);
    }

    private int getLastBotMessage(Message message) {
        return getLastBotMessage(message.getFrom().getId(), message.getChatId());
    }

    private int getLastBotMessage(int userId, long chatId) {
        return stateRepository.findStateByStateId(new StateId(userId, chatId))
                .map(State::getLastBotMessage).orElse(0);
    }
}
