package br.com.miz.versiculos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.telegram.telegrambots.ApiContextInitializer;

@SpringBootApplication
public class VersiculosApplication {

    public static void main(String[] args) {
        ApiContextInitializer.init();
        SpringApplication.run(VersiculosApplication.class, args);
    }

}
