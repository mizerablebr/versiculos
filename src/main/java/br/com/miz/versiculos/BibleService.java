package br.com.miz.versiculos;

import br.com.miz.versiculos.domain.Bible;
import br.com.miz.versiculos.domain.Book;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class BibleService {
    private Map<String, Bible> bibles = new HashMap<>();
    public BibleService() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            this.bibles.put("NVI", new Bible(objectMapper.readValue(new File("./bibles/nvi.json"), Book[].class)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Optional<Bible> getBible(String version) {
        return Optional.ofNullable(bibles.get(version.toUpperCase()));
    }

    public Set<String> getVersions() {
        return this.bibles.keySet();
    }

    public Map<String, String> getBooksAbbrev(Bible bible) {
        return bible.getBooks().stream()
                .collect(
                        LinkedHashMap::new,
                        (map, book) -> map.put(book.getAbbrev(), book.getName()),
                        Map::putAll
                );
    }

    public Map<String, String> getBooksAbbrev(Bible bible, boolean somenteNovoTestamento) {
        return bible.getBooks().stream()
                .skip(somenteNovoTestamento ? 39 : 0)
                .collect(
                        LinkedHashMap::new,
                        (map, book) -> map.put(book.getAbbrev(), book.getName()),
                        Map::putAll
                );
    }
}
