package br.com.miz.versiculos;

import br.com.miz.versiculos.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;

@RestController
public class BibleController {
    private BibleService bibleService;

    public BibleController(BibleService bibleService) {
        this.bibleService = bibleService;
    }

    @GetMapping("{version}/{abbrev}/{chapter}/{verseNumber}")
    public ResponseEntity<ResponseWithStatusString<Verse>> getVerseByAbbrevChapterVerse(
            @PathVariable String version, @PathVariable String abbrev, @PathVariable int chapter, @PathVariable int verseNumber) {
        Optional<Bible> bible = bibleService.getBible(version);
        try {
            // Pega o livro ou retorna exceção
            Book book = bible.map(b -> b.getBookByAbbrev(abbrev).orElseThrow(
                    () -> new IllegalArgumentException(String.format("Livro com abreviatura %s não existe", abbrev))))
                    .orElseThrow(() -> new IllegalArgumentException(String.format("Versão %s da Bíblia não existe", version)));
            // Verifica se o capítulo é válido
            if (!book.isChapterValid(chapter))
                return new ResponseEntity<>(new ResponseWithStatusString<>(new Verse(book, chapter,
                        verseNumber, ""), String.format("Capítulo nº %d é inválido para o livro %s", chapter,
                        book.getName())), HttpStatus.BAD_REQUEST);
            // Verifica se o versículo é válido
            if (!book.isVerseValid(chapter, verseNumber))
                return new ResponseEntity<>(new ResponseWithStatusString<>(new Verse(book, chapter,
                        verseNumber, ""), String.format("Versículo nº %d é inválido para o capítulo %d do livro %s",
                        verseNumber, chapter, book.getName())), HttpStatus.BAD_REQUEST);

            return ResponseEntity.ok(ResponseWithStatusString.ok(book.getVerse(chapter, verseNumber)));

        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(new ResponseWithStatusString<>(new Verse(new Book(), chapter,
                    verseNumber, ""), e.getMessage()), HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping("versions")
    public Set<String> getVersions() {
        return bibleService.getVersions();
    }

    @GetMapping("books/{version}")
    public ResponseEntity<ResponseWithStatusString<Map<String, String>>> getBooksFromVersion(@PathVariable String version) {
        try {
            Bible bible = bibleService.getBible(version).orElseThrow(IllegalArgumentException::new);
            return ResponseEntity.ok(ResponseWithStatusString.ok(bibleService.getBooksAbbrev(bible)));
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(new ResponseWithStatusString<>(new HashMap<>(), e.getMessage()), HttpStatus.BAD_REQUEST);
        }

    }
}
