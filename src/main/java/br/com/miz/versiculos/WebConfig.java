package br.com.miz.versiculos;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.servers.Server;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Collections;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Bean
    public OpenAPI customOpenAPI() {
        Server server = new Server();
        server.setUrl("HTTP://ws.miz.com.br:8080/versiculo");
        return new OpenAPI()
                .servers(Collections.singletonList(server))
                .info(new Info().title("Versículo API").version("0.1")
                        .license(new License().name("Apache 2.0").url("http://springdoc.org")));
    }
}
