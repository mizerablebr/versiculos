package br.com.miz.versiculos.domain;

public class MapperVerse {
    private String text;

    public MapperVerse(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Verse{" +
                "text='" + text + '\'' +
                '}';
    }

}
