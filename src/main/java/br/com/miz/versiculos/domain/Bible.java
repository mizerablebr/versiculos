package br.com.miz.versiculos.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class Bible {
    private List<Book> books = new ArrayList<>();

    public Bible(Book[] books) {
        setBooks(Arrays.asList(books));
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public Optional<Book> getBookByAbbrev(String abbrev) {
        return this.books.stream()
                .filter(b -> b.getAbbrev().toUpperCase().contains(abbrev.toUpperCase()))
                .findFirst();
    }
}
