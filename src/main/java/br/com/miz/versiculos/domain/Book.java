package br.com.miz.versiculos.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

public class Book {
    private String abbrev = "";
    private String name = "";
    private List<List<MapperVerse>> chapters = new ArrayList<>();

    public Book() {
    }

    public String getAbbrev() {
        return abbrev;
    }

    public void setAbbrev(String abbrev) {
        this.abbrev = abbrev;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MapperVerse> getChapter(int number) {
        return  this.chapters.get(number -1);
    }

    public boolean isChapterValid(int chapter) {
        return chapters.size() >= chapter;
    }

    public boolean isVerseValid(int chapter, int verse) {
        return isChapterValid(chapter) && getChapter(chapter).size() >= verse;
    }

    public Verse getVerse(int chapter, int verse) throws IndexOutOfBoundsException {
        return new Verse(this, chapter, verse, getChapter(chapter).get(verse -1).getText());
    }

    public List<List<MapperVerse>> getChapters() {
        return chapters;
    }

    public void setChapters(List<List<MapperVerse>> chapters) {
        this.chapters = chapters;
    }
}
