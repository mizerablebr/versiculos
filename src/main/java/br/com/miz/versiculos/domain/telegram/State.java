package br.com.miz.versiculos.domain.telegram;

import br.com.miz.versiculos.domain.StateId;

import javax.persistence.*;

@Entity
public class State {
    @EmbeddedId
    private StateId stateId;
    private int state;
    private int lastBotMessage;

    public State() {
    }

    public State(StateId stateId, int state, int lastBotMessage) {
        this.stateId = stateId;
        this.state = state;
        this.lastBotMessage = lastBotMessage;
    }

    public StateId getStateId() {
        return stateId;
    }

    public void setStateId(StateId stateId) {
        this.stateId = stateId;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getLastBotMessage() {
        return lastBotMessage;
    }

    public void setLastBotMessage(int lastBotMessage) {
        this.lastBotMessage = lastBotMessage;
    }
}
