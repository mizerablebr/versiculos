package br.com.miz.versiculos.domain;

public class Verse {
    private Book book;
    private int chapter;
    private int number;
    private String text;

    public Verse(Book book, int chapter, int number, String text) {
        this.book = book;
        this.chapter = chapter;
        this.number = number;
        this.text = text;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public int getChapter() {
        return chapter;
    }

    public void setChapter(int chapter) {
        this.chapter = chapter;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getName() {
        return this.book.getName();
    }

    public String getAbbrev() {
        return this.book.getAbbrev();
    }
}
