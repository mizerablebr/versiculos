package br.com.miz.versiculos.domain.telegram;

public class Commands {
    public static final String commandInitChar = "/";
    /// Start command
    public static final String startCommand = commandInitChar + "start";
    /// Cancel command
    public static final String cancelCommand = commandInitChar + "cancel";
    // Stop command
    public static final String STOPCOMMAND = commandInitChar + "stop";
}
