package br.com.miz.versiculos.domain;

public class ResponseWithStatusString<T> {
    private T response;
    private String status;

    public ResponseWithStatusString(T response, String status) {
        this.response = response;
        this.status = status;
    }

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static <T> ResponseWithStatusString<T> ok(T response) {
        return new ResponseWithStatusString<>(response, "Ok");
    }
}
