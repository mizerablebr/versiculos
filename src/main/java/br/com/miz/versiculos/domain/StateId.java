package br.com.miz.versiculos.domain;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class StateId implements Serializable {
    private int userId;
    private long chatId;

    public StateId() {
    }

    public StateId(int userId, long chatId) {
        this.userId = userId;
        this.chatId = chatId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public long getChatId() {
        return chatId;
    }

    public void setChatId(long chatId) {
        this.chatId = chatId;
    }
}
