package br.com.miz.versiculos;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.telegram.telegrambots.ApiContextInitializer;

@SpringBootTest
class VersiculosApplicationTests {
    @BeforeAll
    static void beforeAll() {
        ApiContextInitializer.init();
    }

    @Test
    void contextLoads() {

    }

}
