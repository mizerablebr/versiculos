package br.com.miz.versiculos;

import br.com.miz.versiculos.domain.Bible;
import br.com.miz.versiculos.domain.Book;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.json.JsonTest;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@JsonTest
public class JsonTests {
    @Test
    void ReadTheBible() {
        ObjectMapper objectMapper = new ObjectMapper();
        //objectMapper.disable(DeserializationFeature.UNWRAP_ROOT_VALUE);
        try {
            Book[] books = objectMapper.readValue(new File("./bibles/nvi.json"), Book[].class);
            Bible bible = new Bible(books);
            System.out.println(bible.getBooks().get(0).getChapters().get(0).get(0).getText());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
